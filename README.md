# Validator

**Validator** is a package that provides simple data validation in an object oriented manner

## Requirements 

PHP 5.3+ 

## Installation

### Via composer

TBC

### From source

TBC
 
# Using the Validator package

### Autoloader initialisation
```php
<?php 
use MindTools;

// set up the autoloader - this could be encapsulated ina service container
require_once dirname(__FILE__) . '/Autoloader.php';
$autoloader = new Autoloader;
$autoloader->addNamespace('MindTools', dirname(__FILE__) . '/src');
$autoloader->register();

...
```

### Email validator

```php
<?php
...

try {
    $email_validator = ValidatorFactory::createValidator('MindTools\Validators\Email');
    if (!$email_validator->validate('test@xample.com')) {
    	foreach ($email_validator->getErrors() as $error) {
      		echo $error;
    	}
    }    
} catch (ValidatorFactoryExcpetion $e) {
    // catch and handle ValidatorFactory specific exceptions 
}

...

```

#### Setting validator options 

Some validators can be configured by setting options 

```php
<?php
...

// Set options via factory
$options = array(
	'callback' => 'name_of_callback_function'
);
$validator = ValidatorFactory::createValidator('MindTools\Validator\Callback', $options);

// set options after factory
$validator->setOptions($options);

...
```

### Extending the library with Callback validators

#### Closure

```php
<?php
...

try {

    /**
     * Define a closure that will validate the data.
     * Must return boolean and accept an array for 
     * the validator options and an error array input
     * by ref so that the Validator can raise an error
     */
    $callback = function ($data, $options, &$error)
    {
        if (is_array($errors) === false) {
            $errors = array();
        }

        if (is_bool($data) === false) {
            $error = 'Data must be of type boolean';
            return false;
        }

        return true;          
    }

    $callback_validator = ValidatorFactory::createValidator('MindTools\Validators\Callback');
    $callback_validator->setOptions(array('callback', $callback));
    
    if ($callback_validator->validate(true) === false) {
    	echo $callback_validator->getErrorMessage();
    }    
} catch (ValidatorFactoryExcpetion $e) {
    // catch and handle ValidatorFactory specific exceptions 
} 

...
  
```

#### Object method

```php
<?php
...

try {

	$my_object = new MyObject();
	$callback_validator = ValidatorFactory::createValidator('callback');
	$callback_validator->setOptions(array('callback', array($my_object, 'my_method')));
	if ($callback_validator->validate($some_var) === false) {
		echo $callback_validator->getErrorMessage();
	}	
} catch (ValidatorFactoryException) {
	// Catch and handle any ValidatorFactoryExceptions
}
...
```