<?php
/**
 * @package MindTools
 * @version 1.0
 */

namespace MindTools;

/**
 * General purpose autoloader class
 *
 * @link http://www.php-fig.org/psr/psr-4/examples/
 */
class Autoloader
{
    protected $prefixes = array();

    /**
     * Register loader with autoload stack
     *
     * @return void
     */
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    /**
     * Registers a base directory against a namespace
     *
     * @param string $prefix The namespace prefix
     * @param string $base_dir A directory to register against the namespace
     * @return void
     */
    public function addNamespace($prefix, $base_dir)
    {
        $prefix = trim($prefix, '\\') . '\\';

        $base_dir = rtrim($base_dir, DIRECTORY_SEPARATOR) . '/';

        if (isset($this->prefixes[$prefix]) === false) {
            $this->prefixes[$prefix] = array();
        }

        array_push($this->prefixes[$prefix], $base_dir);
    }

    /**
     * Load the classfile for given class
     *
     * @param String $class the full qualified classname
     * @return mixed the mapped file anem on success or boolean on false or failure
     */
    public function loadClass($class)
    {
        $prefix = $class;

        // step backwards through each segment of the fully-qualified classname
        // Note use of strrpos not strpos
        while (false !== $pos = strrpos($prefix, '\\')) {
            $prefix = substr($class, 0, $pos + 1);
            
            // strip prefix to get relative class name
            $relative_class = substr($class, $pos +1);

            // try and load the a mapped file
            $mapped_file = $this->loadMappedFile($prefix, $relative_class);
            if ($mapped_file) {
                return $mapped_file;
            }

            // remove trailing namespace separator for next iteration
            // of strpos()
            $prefix = rtrim($prefix, '\\');
        }

        // never found a file
        return false;
    }

    /**
     * Load the mapped file for a namespace prefix
     *
     * @param string $prefix
     * @param string $relative_class
     * @return mixed Boolean flse if no mapped file can be loaded, or the
     * name of the mapped file file that was loaded.
     */
    protected function loadMappedFile($prefix, $relative_class)
    {
        if (isset($this->prefixes[$prefix]) === false) {
            return false;
        }

        foreach ($this->prefixes[$prefix] as $base_dir) {
            $file = $base_dir
                . str_replace('\\', '/', $relative_class)
                . '.php';

            if ($this->requireFile($file)) {
                return $file;
            }
        }

        // didn't find a file
        return false;
    }


    /**
     * If a file exists, require it from the file system
     *
     * @param string $file The file to require.
     * @return bool True if the file exists, false if not.
     */
    protected function requireFile($file)
    {
        if (file_exists($file)) {
            require $file;
            return true;
        }
        return false;
    }
}

// vim: nu ts=4 sw=4 expandtab nobomb
