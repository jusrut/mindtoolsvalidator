<?php
/**
 * @package     MindTools
 * @subpackage  Validator
 */

namespace MindTools\Validator;

/**
 * Base class for validators
 * @abstract
 */
abstract class AbstractValidator
{

    /**
     * @var array
     * @access protected
     */
    protected $options = array();

    /**
     * @var array Array containing error messages
     * @access protected
     */
    protected $errors = array();

    /**
     * Sets options for validator
     *
     * Chacks for explicit setter methods for each option
     * Returns ValidatorBase for fluent interface
     * Will throw an InvalidArgumentException if the options are not an array
     * @param array $options
     * @return ValidatorBase
     * @throws InvalidArgumentException
     */
    public function setOptions($options)
    {
        if (!is_array($options)) {
            $msg = "Options passed to ". __METHOD__ ." must be of type array";
            throw new InvalidArgumentException($msg);
        }
        
        foreach ($options as $key => $val) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($val);
            } else {
                $this->options[$key] = $val;
            }
        }

        return $this;
    }

    /**
     * Returns contents of error array
     *
     * @return array Error array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Adds an error message to the error stack
     *
     * @return void
     * @access protected
     */
    protected function addError($error_message)
    {
        array_push($this->errors, $error_message);
    }
}

// vim: nu ts=4 sw=4 expandtab
