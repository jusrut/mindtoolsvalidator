<?php

namespace MindTools\Validator;

class Callback extends AbstractValidator implements ValidatorInterface
{

    public function validate($data)
    {
        // check the callback is callable - should probably do some
        // further testing on the callable parameters etc. maybe with
        // some reflection
        if (isset($this->options['callback']) === false) {
            throw new InvalidArgumentException('Callback has not been set');
        }

        // prime the error stack to be passed by reference into the callback
        $errors = array();

        // Get return value from callback
        // passing a reference through the array should be okay
        $valid = call_user_func_array($callback, array(
            $this->options,
            &$errors
        ));

        // check the errors and then add them to our stack if necesasry
        if (is_array($errors)) {
            foreach ($errors as $error) {
                $this->addError($error);
            }
        }

        // return callback value
        return $valid;
    }

    /**
     * Set the callback
     *
     * We should use reflection here to make sure callback accepts necessary
     * arguments
     * @param callable $callback
     */
    public function setCallback($callback)
    {
        if (is_callable($callback) === false) {
            throw new InvalidArgumentException('Callback passed is not callable');
        }

        $this->options['callback'] = $callback;
    }
}

// vim: nu ts=4 sw=4 expandtab
