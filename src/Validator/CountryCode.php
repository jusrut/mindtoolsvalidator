<?php
/**
 * @package MindTools
 * @subpackage Validator
 * @author justin@justinrutherford.com
 */

namespace MindTools\Validator;

/**
 * Simple ISO3166 country code validator
 *
 * Makes use of third party ISO3166 library
 * @link https://github.com/julien-c/iso3166
 */
class CountryCode extends AbstractValidator implements ValidatorInterface
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $autoloader = new \MindTools\Autoloader;
        $autoloader->addNamespace('ISO3166', __DIR__ . '/../vendor/julien-c/iso3166/src');
        $autoloader->register();
    }

    /**
     * Checks whether given code validates against ISO3166 library
     *
     * @param string $code the code to check
     * @return bool
     */
    public function validate($code)
    {
        if (\Iso3166\Codes::isValid($code) === false) {
            $msg = sprintf('Supplied country code "%s" is not valid', $code);
            $this->addError($msg);
            return false;
        }

        return true;
    }
}

// vim: nu ts=4 sw=4 expandtab
