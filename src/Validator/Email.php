<?php
/**
 * @package     MindTools
 * @subpackage  Validator
 * @Author      justin@justinrutherford.com
 */

namespace MindTools\Validator;

/**
 * Simple email validation class
 */
class Email extends AbstractValidator implements ValidatorInterface
{
    /**
     * Chacks validity of given email address
     *
     * This method makes use of PHP's built in filter_var(). There are
     * reported issues with this function not being compliant with
     * email standards (@see http://php.net/manual/en/filter.examples.validation.php)
     *
     * @param string $email The email address to validate
     * @return boolean
     */
    public function validate($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $this->addError(sprintf('Email address "%s" is not valid', $email));
            return false;
        }

        return true;
    }
}

// vim: nu ts=4 sw=4 expandtab
