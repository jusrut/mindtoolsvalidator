<?php
/**
 * @package MindTools
 * @subpackage Validator
 * @author justin@justinrutherford.com
 */

namespace MindTools\Validator;

/**
 * Validate a string for password strength
 *
 * Contains parts of code from cook book
 * @link http://docstore.mik.ua/orelly/webprog/pcook/ch14_06.htm
 */
class PasswordStrength extends AbstractValidator implements ValidatorInterface
{

    /**
     * Default option values
     *
     * @var array
     */
    protected $options = array(
        'minLength' => 12,
        'minNumbers' => 2,
        'minUpper' => 2,
        'minLower' => 2,
        'minSpecial' => 2
    );

    public function validate($password)
    {
        $uppercase = 0;
        $lowercase = 0;
        $numbers = 0;
        $special = 0;

        for ($i = 0; $i < strlen($password); $i++) {
            $char = substr($pass, $i, 1);
            
            if (preg_match('/^[[:upper:]]$/', $char)) {
                $uppercase++;
            } elseif (preg_match('/^[[:lower:]]$/', $char)) {
                $lowercase++;
            } elseif (preg_match('/^[[:digit:]]$/', $char)) {
                $numbers++;
            } else {
                $special++;
            }
        }

        if (strlen($password) < $this->options['minLength']) {
            $this->addError('The password is too short.');
        }
    
        if ($uppercase < $this->options['minUppercase']) {
            $msg = sprintf(
                'The password must contain %s uppercase characters',
                $this->options['minUppercase']
            );
            $this->addError($msg);
        }

        if ($lowercase < $this->options['minLowercase']) {
            $msg = sprintf(
                'The password must contain %s lowercase characters',
                $this->options['minLowercase']
            );
            $this->addError($msg);
        }

        if ($numbers < $this->options['minNumbers']) {
            $msg = sprintf(
                'The password must contain %s numeric characters',
                $this->options['minNumbers']
            );
            $this->addError($msg);
        }

        if ($special < $this->options['minSpecials']) {
            $msg = sprintf(
                'The password must contain %s special characters',
                $this->options['minSpecials']
            );
            $this->addError($msg);
        }

        return (count($this->errors) == 0);
    }
}

// vim: nu ts=4 sw=4 expandtab
