<?php
/**
 * @package     MindTools
 * @subpackage  Validator
 * @Author  justin@justinrutherford.com
 */

namespace MindTools\Validator;

/**
 * Simple string length validator
 *
 * @property int min
 * @property int max
 *
 * To use this validator you must set the min and max values of the options
 * array.
 *
 * <code>
 *    $options = array(
 *     'min' => 3,
 *     'max' => 6
 *    );
 *    $v = ValidatorFactory::create('MindTools\Validator\StringLength', $options);
 * </code>
 */
class StringLength extends AbstractValidator implements ValidatorInterface
{

    /**
     * Checks that string length is within the given range
     *
     * @param string The string to check
     * @throws InvalidArgumentException if the range has not been set properly
     * @return bool
     */
    public function validate($string)
    {
        if (isset($this->options['min']) === false) {
            throw new InvalidArgumentException('Min value not given');
        }

        if (isset($this->options['max']) === false) {
            throw new InvalidArgumentException('Max value not given');
        }

        $options = array(
            'options' => array(
                'min_range' => $this->options['min'],
                'max_range' => $this->options['max']
            )
        );

        if (filter_var(strlen($string), FILTER_VALIDATE_INT, $options) === false) {
            $msg = 'The string length was not in the range %s-%s';
            $this->addError(sprintf(
                $msg,
                $this->options['min'],
                $this->options['max']
            ));
            return false;
        }

        return true;
    }

    /**
     * Sets the min value option
     *
     * @param int $min
     * @throws InvalidArguemtnException
     */
    public function setMin($min)
    {
        if (is_int($min) === false) {
            throw new InvalidArgumentException('Max value must be an integer');
        }

        $this->options['min'] = $min;
    }

    /**
     * Sets the max value option
     *
     * @param int $max
     * @throws InvalidArgumentException
     */
    public function setMax($max)
    {
        if (is_int($max) === false) {
            throw new InvalidArgumentException('Max value must be an integer');
        }
        $this->options['max'] = $max;
    }
}

// vim: nu ts=4 sw=4 expandtab
