<?php
/**
 * @package     MindTools
 * @subpackage  Validator
 * @Author      justin@justinrutherford.com
 */

namespace MindTools\Validator;

/**
 * Simple URL validation rule
 */
class Url extends AbstractValidator implements ValidatorInterface
{
    /**
     * Checks valididty of URL
     *
     * @param string $url The URL to validate
     * @return boolean
     */
    public function validate($url)
    {
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            $this->addError(sprintf('URL "%s" is not valid', $url));
            return false;
        }

        return true;
    }
}

// vim: nu ts=4 sw=4 expandtab
