<?php
/**
 * @package     MindTools
 * @subpackage  Validators
 */

namespace MindTools\Validator;

/**
 * Factory class for creating and configuring validators
 */
class ValidatorFactory
{

    /**
     * @var Autoloader
     * @static
     * @access protected
     */
    protected static $autoloader;

    /**
     * @static
     * @access public
     * @param String $name
     * @param Array $options optional array containing configuration options for the validator
     * @throws ValidatorFactoryException
     * @return Validator
     */
    public static function createValidator($name, $options = null)
    {
        self::checkValidatorClass($name);

        try {
            $validator = new $name;

            if (null != $options) {
                $validator->setOptions($options);
            }

            return $validator;
        } catch (InvalidArgumentException $e) {
            // rethrow but as ValidatorFactoryException
            throw new ValidationFactoryException($e->getMessage());
        }
    }

    /**
     * CHecks whether the given class exists and implements the Validator iterface
     * Throws ValidatorFActoryException if class is not found or is not valid
     * @param String $name the class name to check
     * @return void
     * @throws ValidatorFactoyExcecption
     * @access protected
     * @static
     */
    protected static function checkValidatorClass($name)
    {
        if (!class_exists($name)) {
            throw new ValidatorFactoryException("Class \"{$name}\" does not exist.");
        }
        if (!in_array(__NAMESPACE__.'\\ValidatorInterface', class_implements($name))) {
            $msg = "Class \"{$name}\" does not implement interface Validator";
            throw new ValidatorFactoryException($msg);
        }
    }
}

// vim: nu ts=4 sw=4 expandtab
