<?php
/**
 * @package MindTools
 * @subpackage Validators
 */

namespace MindTools\Validator;

/**
 * Validator interface
 */
interface ValidatorInterface
{
    public function validate($data);
    public function setOptions($options);
    public function getErrors();
}

// vim: nu ts=4 sw=4 expandtab
