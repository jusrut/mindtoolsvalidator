<?php
require_once __DIR__ . '/Autoloader.php';
$autoloader = new \MindTools\Autoloader;
$autoloader->addNamespace('MindTools', __DIR__);
$autoloader->register();
