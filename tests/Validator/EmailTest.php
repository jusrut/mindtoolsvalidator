<?php

use \MindTools\Validator\ValidatorFactory as Factory;

class EmailTest extends PHPUnit_Framework_TestCase
{

	public function setUp() 
	{
		$this->validator = Factory::createValidator('\MindTools\Validator\Email');
	}

	public function testValidateEmail()
	{
		$this->assertTrue($this->validator->validate('test@example.com'));
	}

	public function testValidateInvalidEmail()
	{
		$this->assertFalse($this->validator->validate('too@many@symbols.com'));
	}

	public function testValidateEmptyString()
	{
		$this->assertFalse($this->validator->validate(''));
	}
}
