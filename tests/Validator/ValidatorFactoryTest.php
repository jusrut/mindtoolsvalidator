<?php

use \MindTools\Validator\ValidatorFactory as Factory;

class ValidatorFactoryTest extends PHPUnit_Framework_TestCase
{
	public function testCanCreateEmailValidator()
	{
		$v = Factory::createValidator('MindTools\Validator\Email');
		$this->assertInstanceOf('\MindTools\Validator\Email', $v);
	}

	public function testCanCreateUrlValidator()
	{
		$v = Factory::createValidator('MindTools\Validator\Url');
		$this->assertInstanceOf('MindTools\Validator\Url', $v);
	}

	public function testCanCreateStringLengthValidator()
	{
		$v = Factory::createValidator('MindTools\Validator\StringLength');
		$this->assertInstanceOf('MindTools\Validator\StringLength', $v);
	}

	public function testCanCreatePasswordStrengthValidator()
	{
		$v = Factory::createValidator('MindTools\Validator\PasswordStrength');
		$this->assertInstanceOf('MindTools\Validator\PasswordStrength', $v);
	}

	public function testCanCreateCountryCodeValidator()
	{
		$v = Factory::createValidator('MindTools\Validator\CountryCode');
		$this->assertInstanceOf('MindTools\Validator\CountryCode', $v);
	}

	public function testCanCreateCallbackValidator()
	{
		$v = Factory::createValidator('MindTools\Validator\Callback');
		$this->assertInstanceOf('MindTools\Validator\Callback', $v);
	}

}

